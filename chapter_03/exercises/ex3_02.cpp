/*
Write a program in C++ that converts from miles to kilometers. Your
program should have a reasonable prompt for the user to enter a number
of miles. Hint: There are 1.609 kilometers to the mile
*/

#include <iostream>
using namespace std;

int main()
{
    double KILOMETERS_PER_MILE = 1.609;
    double miles;
    double kilometers;

    cout << "Enter number of miles to convert: ";
    cin >> miles;

    kilometers = miles * KILOMETERS_PER_MILE;

    cout << "Answer: " << kilometers << '\n';

    return 0;
}