/*
Write a program that prompts the user to enter some number of pen-
nies (1-cent coins), nickels (5-cent coins), dimes (10-cent coins), quar-
ters (25-cent coins), half dollars (50-cent coins), and one-dollar coins
(100-cent coins). Query the user separately for the number of each size
coin, e.g., “How many pennies do you have?” Then your program should
print out something like this:
You have 23 pennies.
You have 17 nickels.
You have 14 dimes.
You have 7 quarters.
You have 3 half dollars.
The value of all of your coins is 573 cents.
Make some improvements: if only one of a coin is reported, make the
output grammatically correct, e.g., 14 dimes and 1 dime (not 1 dimes).
Also, report the sum in dollars and cents, i.e., $5.73 instead of 573 cents.
*/

#include <iostream>
using namespace std;

int main()
{
    int pennies, nickels, dimes, quarters, half_dollar;
    int total_cents = 0;
    double dollars = 0.0;

    cout << "Enter number of pennies: ";
    cin >> pennies;

    cout << "Enter number of nickels: ";
    cin >> nickels;

    cout << "Enter number of dimes: ";
    cin >> dimes;

    cout << "Enter number of quarters: ";
    cin >> quarters;

    cout << "Enter number of half dollars: ";
    cin >> half_dollar;

    total_cents = total_cents + pennies;
    total_cents = total_cents + (nickels * 5);
    total_cents = total_cents + (dimes * 10);
    total_cents = total_cents + (quarters * 25);
    total_cents = total_cents + (half_dollar * 50);


    // display amount of coins
    cout << "\nYou have " << pennies << " pennie(s)\n";
    cout << "You have " << nickels << " nickel(s)\n";
    cout << "You have " << dimes << " dime(s)\n";
    cout << "You have " << quarters << " quarter(s)\n";
    cout << "You have " << half_dollar << " half dollar(s)\n";

    cout << "The value of all your coins is " << total_cents << " cents\n";

    dollars = total_cents / 100.0;
    cout << "The sum in dollars and cents is $" << dollars << '\n';

    return 0;
}