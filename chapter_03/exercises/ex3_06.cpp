/*
Write a program that prompts the user to enter three integer values, and
then outputs the values in numerical sequence separated by commas. So,
if the user enters the values 10 4 6, the output should be 4, 6, 10. If two
values are the same, they should just be ordered together. So, the input
4 5 4 should give 4, 4, 5.
*/

#include <iostream>
using namespace std;

int main()
{
    int val1 = 0, val2 = 0, val3 = 0;
    int first = 0, mid = 0, last = 0;

    cout << "Please enter three integer values: ";
    cin >> val1 >> val2 >> val3;

    cout << '\n';

    if (val1 < val2 && val1 < val3) {
        first = val1;
        if (val2 < val3) {
            mid = val2;
            last = val3;
        }
        else {
            mid = val3;
            last = val2;
        }
    } 
    else if (val2 < val3) {
        first = val2;
        if (val1 < val3) {
            mid = val1;
            last = val3;
        }
        else {
            mid = val3;
            last = val1;
        }
    } else {
        first = val3;
        
        if (val2 < val1) {
            mid = val2;
            last = val1;
        }
        else {
            mid = val1;
            last = val2;
        }
    }

    cout << first << ", " << mid << ", " << last << '\n';

    return 0;
}