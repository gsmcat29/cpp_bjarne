/*
Modify the program above to ask the user to enter floating-point values
and store them in double variables. Compare the outputs of the two pro-
grams for some inputs of your choice. Are the results the same? Should
they be? What’s the difference?
*/

#include <iostream>
using namespace std;

int main()
{
    double val1 = 0;
    double val2 = 0;

    cout << "Enter two values: ";
    cin >> val1 >> val2;

    cout << "Results: =======================\n";
    if (val1 > val2) {
        cout << val1 << " is larger than " << val2 << '\n';
    }
    else {
        cout << val1 << " is smaller than " << val2 << '\n';
    }

    cout << "val1 + val2 = " << val1 + val2 << '\n';
    cout << "val1 - val2 = " << val1 - val2 << '\n';
    cout << "val1 * val2 = " << val1 * val2 << '\n';
    cout << "val1 / val2 = " << val1 / val2 << '\n';

    return 0;
}