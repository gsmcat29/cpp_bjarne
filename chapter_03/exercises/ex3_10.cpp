/*
Write a program that takes an operation followed by two operands and
outputs the result. For example:
+ 100 3.14
*45
Read the operation into a string called operation and use an
if-statement to figure out which operation the user wants, for example,
if (operation=="+"). Read the operands into variables of type double.
Implement this for operations called +, –, *, /, plus, minus, mul, and div
with their obvious meanings.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string operation;
    double value1 = 0, value2 = 0;
    double result = 0;

    cout << "Enter operation folowing formt: operator value1 value2: ";
    cin >> operation >> value1 >> value2;

    if (operation == "+" || operation == "plus") {
        result = value1 +  value2;
        cout << "Ans: " << result << '\n';
    }
    else if (operation == "-" || operation == "minus") {
        result = value1 - value2;
        cout << "Ans: " << result << '\n';
    }
    else if (operation == "*" || operation == "mul") {
        result = value1 * value2;
        cout << "Ans: " << result << '\n';
    }
    else if (operation == "/" || operation == "div") {
        result = value1 / value2;
        cout << "Ans: " << result << '\n';
    }

    return 0;
}