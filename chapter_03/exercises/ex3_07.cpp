/*
Do exercise 6, but with three string values. So, if the user enters the val-
ues Steinbeck, Hemingway, Fitzgerald, the output should be Fitzgerald,
Hemingway, Steinbeck.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string val1,val2, val3;
    string first, mid, last;

    cout << "Please enter three string values: ";
    cin >> val1 >> val2 >> val3;

    cout << '\n';

    if (val1 < val2 && val1 < val3) {
        first = val1;
        if (val2 < val3) {
            mid = val2;
            last = val3;
        }
        else {
            mid = val3;
            last = val2;
        }
    } 
    else if (val2 < val3) {
        first = val2;
        if (val1 < val3) {
            mid = val1;
            last = val3;
        }
        else {
            mid = val3;
            last = val1;
        }
    } else {
        first = val3;
        
        if (val2 < val1) {
            mid = val2;
            last = val1;
        }
        else {
            mid = val1;
            last = val2;
        }
    }

    cout << first << ", " << mid << ", " << last << '\n';

    return 0;
}