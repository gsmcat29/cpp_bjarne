/*
Write a program that converts spelled-out numbers such as “zero” and
“two” into digits, such as 0 and 2. When the user inputs a number, the
program should print out the corresponding digit. Do it for the values 0,
1, 2, 3, and 4 and write out not a number I know if the user enters some-
thing that doesn’t correspond, such as stupid computer!.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    int number = 0;
    string spell_out;

    cout << "Please enter a number: ";
    cin >> number;

    if (number == 0) {
        spell_out = "zero";
    }
    else if (number == 1) {
        spell_out = "one";
    }
    else if (number == 2) {
        spell_out = "two";
    }
    else if (number == 3) {
        spell_out = "three";
    }
    else if (number == 4) {
        spell_out = "four";
    }
    else {
        spell_out = "Not a number. Stupid computer";
    }

    cout << spell_out << '\n';

    return 0;
}