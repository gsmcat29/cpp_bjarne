/*
Write a program to test an integer value to determine if it is odd or
even. As always, make sure your output is clear and complete. In other
words, don’t just output yes or no. Your output should stand alone,
like The value 4 is an even number. Hint: See the remainder (modulo)
operator in §3.4.
*/

#include <iostream>
using namespace std;

int main()
{
    int number = 0;

    cout << "Please enter a number: ";
    cin >> number;

    // using modulo operator, number % 2 == 0
    // !(number & 1), trick
    if (!(number & 1)) {
        cout << "The value " << number << " is an even number\n";
    }
    else {
        cout << "The value " << number << " is an odd number\n";
    }

    return 0;
}