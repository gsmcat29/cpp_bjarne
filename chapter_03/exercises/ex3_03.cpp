/*
Write a program that doesn’t do anything, but declares a number of vari-
ables with legal and illegal names (such as int double = 0;), so that you
can see how the compiler reacts.
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    int double = 0;
    string int = "???";
    double string = 3.14;
    
}