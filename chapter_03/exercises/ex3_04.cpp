/*
Write a program that prompts the user to enter two integer values. Store
these values in int variables named val1 and val2. Write your program to
determine the smaller, larger, sum, difference, product, and ratio of these
values and report them to the user
*/

#include <iostream>
using namespace std;

int main()
{
    int val1 = 0;
    int val2 = 0;

    cout << "Enter two values: ";
    cin >> val1 >> val2;

    cout << "Results: =======================\n";
    if (val1 > val2) {
        cout << val1 << " is larger than " << val2 << '\n';
    }
    else {
        cout << val1 << " is smaller than " << val2 << '\n';
    }

    cout << "val1 + val2 = " << val1 + val2 << '\n';
    cout << "val1 - val2 = " << val1 - val2 << '\n';
    cout << "val1 * val2 = " << val1 * val2 << '\n';
    cout << "val1 / val2 = " << val1 / val2 << '\n';

    return 0;
}