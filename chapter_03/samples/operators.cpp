// simple program to exercise operators
#include <cmath>
#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << "Please enter a floating-point value: ";
    double n;
    cin >> n;
    cout << "n == " << n
         << "\nn+1 == " << n + 1
         << "\nthree times n == " << 3 * n
         << "\ntwice n == " << n + n
         << "\nhalf of n == " << n/2
         << "\nsquare rot of n == " << sqrt(n)
         << '\n'; // name for newline (end of line) in output
}