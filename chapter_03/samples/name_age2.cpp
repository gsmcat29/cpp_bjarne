// read name and age (2nd version)
#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << "Please enter your first name and age\n";
    string first_name = "???";      // string variable
    int age = 0;                    // integer variable

    cin >> first_name >> age;       // read string followed by integer
    cout << "Hello, " << first_name << " (age " << age << ")\n";
}