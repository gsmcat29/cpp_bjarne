/*
Write a description of how to get from the front door of your dorm room,
apartment, house, whatever, to the door of your classroom (assuming you
are attending some school; if you are not, pick another target). Have a
friend try to follow the instructions and annotate them with improve-
ments as he or she goes along. To keep friends, it may be a good idea to
“field test” those instructions before giving them to a friend.
*/

#include "std_lib_facilities.h"

int main()          
{
    cout << "Follow the instructions to get from the front door to classroom\n";
    cout << "1) Get up from the chair\n";
    cout << "2) Prepare your backpack for school\n";
    cout << "3) Use any type of transportation to arrive to your school\n";
    cout << "4) Walk to your classroom\n";
    cout << "5) Open your door\n";
    
    return 0;
}