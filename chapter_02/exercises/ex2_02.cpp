/*
Expanding on what you have learned, write a program that lists the in-
structions for a computer to find the upstairs bathroom, discussed in §2.1.
Can you think of any more steps that a person would assume, but that a
computer would not? Add them to your list. This is a good start in “think-
ing like a computer.” Warning: For most people, “go to the bathroom” is
a perfectly adequate instruction. For someone with no experience with
houses or bathrooms (imagine a stone-age person, somehow transported
into your dining room) the list of necessary instructions could be very
long. Please don’t use more than a page. For the benefit of the reader, you
may add a short description of the layout of the house you are imagining.
*/
#include "std_lib_facilities.h"

int main()          
{
    cout << "Follow the instructions  to find the upstairs bathroom\n";
    cout << "1) Get up from the chair\n";
    cout << "2) Walk to the stairs\n";
    cout << "3) Take the stairs up to the second floor entrance\n";
    cout << "4) Walk in the hallway until you find the door\n";
    cout << "5) Open the door\n";
    cout << "6) Use the bathroom depending of your necesities\n";
    cout << "7) Wash your hand afterwards\n";
    cout << "8) Return to the chair reverting steps 5 to 1\n";
    
    return 0;
}