/*
Write a definition for each of the terms from “Terms.” First try to see if you
can do it without looking at the chapter (not likely), then look through the
chapter to find definitions. You might find the difference between your
first attempt and the book’s version interesting. You might consult some
suitable online glossary, such as www.stroustrup.com/glossary.html. By
writing your own definition before looking it up, you reinforce the learn-
ing you achieved through your reading. If you have to reread a section to
form a definition, that just helps you to understand. Feel free to use your
own words for the definitions, and make the definitions as detailed as
you think reasonable. Often, an example after the main definition will be
helpful. You may like to store the definitions in a file so that you can add
to them from the “Terms” sections of later chapters.
*/

#include <iostream>
using namespace std;

int main()
{
    cout << "Terms:\n";

    cout << "//: Comments\n";
    cout << "<<: Stream operators used for output\n";
    cout << "C++: Programming language created by Bjarne Stroustrup\n";
    cout << "comment: Used to explain code, this is not display at compiling\n";
    cout << "compiler: Translates code into another language\n";
    cout << "compile-time error: Error that appears during compiling\n";
    cout << "cout: Used toprint something to the screen\n";
    cout << "executable: Binary program that executes a piece of code\n";
    cout << "function: Piece  of code that do a fraction of a program\n";
    cout << "header: A library where functions are stored\n";
    cout << "IDE: A tools used for programming\n";
    cout << "#include: A directive to include headers\n";
    cout << "library: A set of functions that do something\n";
    cout << "linker: Combines object files into  executable program\n";
    cout << "main(): Init piece of code for C++\n";
    cout << "object code: Product of the compiler\n";
    cout << "output: Displays the result of a code\n";
    cout << "program: A set of instructions that do something\n";
    cout << "source code: The main code of a program\n";
    cout << "statement: A code statement\n";

    return 0;
}