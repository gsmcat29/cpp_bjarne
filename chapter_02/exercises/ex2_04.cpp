/*
Find a good cookbook. Read the instructions for baking blueberry muf-
fins (if you are in a country where “blueberry muffins” is a strange, exotic
dish, use a more familiar dish instead). Please note that with a bit of
help and instruction, most of the people in the world can bake delicious
blueberry muffins. It is not considered advanced or difficult fine cooking.
However, for the author, few exercises in this book are as difficult as this
one. It is amazing what you can do with a bit of practice.
*/

#include <iostream>
using namespace std;

int main()
{
    cout << "Creamy Caramel Flan\n";
    cout << "Total Time Prep: 25 min. + standing Bake: 50 min. + chilling\n";
    cout << "Ingridients:\n\n";
    cout << "3/4 cup sugar\n";
    cout << "1/4 cup water\n";
    cout << "1 package (8 ounces) cream cheese, softened\n";
    cout << "5 large eggs\n";
    cout << "1 can (14 ounces) sweetened condensed milk\n";
    cout << "1 can (12 ounces) evaporated milk\n";
    cout << "1 teaspoon vanilla extract\n";
    cout << "\n\n";
    cout << "Directions:\n";
    cout << "In a heavy saucepan, cook sugar and water over medium-low heat\n";
    cout << "until melted and golden, about 15 minutes. Brush down crystals\n";
    cout << "on the side of the pan with additional water as necessary.\n";
    cout << "Quickly pour into an ungreased 2-qt. round baking or souffle\n";
    cout << "dish, tilting to coat the bottom; let stand for 10 minutes.\n";
    cout << "\n";
    cout << "Preheat oven to 350°. In a bowl, beat the cream cheese until smooth.";
    cout << "\nBeat in eggs, 1 at a time, until thoroughly combined.\n";
    cout << "Add remaining ingredients; mix well.\n";
    cout << "Pour over caramelized sugar. \n";
    cout << "\n\n";
    cout << "Place the dish in a larger baking pan.\n";
    cout << "Pour boiling water into larger pan to a depth of 1 in. Bake until\n";
    cout << "the center is just set (mixture will jiggle), 50-60 minutes.\n";
    cout << "\n\n";
    cout << "Remove dish from a larger pan to a wire rack; cool for 1 hour.\n";
    cout << "Refrigerate overnight.\n";
    cout << "\n\n";
    cout << "To unmold, run a knife around edge and invert onto a large rimmed\n";
    cout << "serving platter. Cut into wedges or spoon onto dessert plates;\n";
    cout << "spoon sauce over each serving.\n";

    return 0;
}