/*
Change the program to output the two lines

Hello, programming!

Here we go!
*/

#include "std_lib_facilities.h"

int main()          // C++ programs start by executing the function main
{
    cout << "Hello, programming!\n";      // output hello, world
    cout << "Here we go!\n";
    
    return 0;
}