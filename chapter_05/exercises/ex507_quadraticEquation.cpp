/*
Quadratic equations are of the form

    a ⋅x2 + b ⋅x + c = 0

To solve these, one uses the quadratic formula:

x= −b ± b 2 − 4ac / 2a

There is a problem, though: if b 2–4ac is less than zero, then it will fail.
Write a program that can calculate x for a quadratic equation. Create a
function that prints out the roots of a quadratic equation, given a, b, c.
When the program detects an equation with no real roots, have it print
out a message. How do you know that your results are plausible? Can
you check that they are correct?
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double a = 0;
    double b = 0;
    double c = 0;
    // quadratic formula
    double x0 = 0;
    double x1 = 0;

    cout << "Please enter the arguments for a, b, and c: ";
    cin >> a >> b >> c;

    double discriminant = (b * b) - 4*(a * c);

    if (discriminant > 0) {
        cout << "We are going to obtain two real solutions:\n";
        x0 = (-b + sqrt(discriminant)) / (2 * a);
        x1 = (-b - sqrt(discriminant)) / (2 * a);

        cout << "Root0 = " << x0 << '\n';
        cout << "Root1 = " << x1 << '\n';
    }
    else if (discriminant == 0) {
        cout << "We are going to obtain one solution:\n";
        x0 = (-b + sqrt(discriminant)) / (2 * a);
        cout << "Root: " << x0 << '\n';
    }
    else {
        cout << "We are going to obtain complex solution:\n";
        cout << "This program does not support complex numbers!!\n";
        exit(1);
    }


}
