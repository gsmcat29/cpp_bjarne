/*
Read (day-of-the-week,value) pairs from standard input. For example:

    Tuesday 23 Friday 56 Tuesday –3 Thursday 99

Collect all the values for each day of the week in a vector<int>. Write out
the values of the seven day-of-the-week vectors. Print out the sum of the
values in each vector. Ignore illegal days of the week, such as Funday,
but accept common synonyms such as Mon and monday. Write out the
number of rejected values.
*/

#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    vector<int> mondays;
    vector<int> tuesdays;
    vector<int> wednesdays;
    vector<int> thursdays;
    vector<int> fridays;
    vector<int> saturdays;
    vector<int> sundays;

    int value = 0;
    int rejected = 0;
    string day = " ";

    int sum_mon = 0;
    int sum_tue = 0;
    int sum_wed = 0;
    int sum_thu = 0;
    int sum_fri = 0;
    int sum_sat = 0;
    int sum_sun = 0;


    while(cin >> day >> value) {

        if (day == "Monday" || day == "Mon" || day == "monday") {
            mondays.push_back(value);
        }
        else if (day == "Tuesday" || day == "Tue" || day == "tuesday") {
            tuesdays.push_back(value);
        }
        else if (day == "Wednesday" || day == "Wed" || day == "wednesday") {
            wednesdays.push_back(value);
        }
        else if (day == "Thursday" || day == "Thu" || day == "thursday") {
            thursdays.push_back(value);
        }
        else if (day == "Friday" || day == "Fri" || day == "friday") {
            fridays.push_back(value);
        }
        else if (day == "Saturday" || day == "Sat" || day == "saturday") {
            saturdays.push_back(value);
        }
        else if (day == "Sunday" || day == "Sun" || day == "sunday") {
            sundays.push_back(value);
        }
        else
            ++rejected;

    }

    cout << "Number of rejections: " << rejected << '\n';

    // sum of values
    
    for (size_t i = 0; i < mondays.size(); ++i) {
        sum_mon = sum_mon + mondays[i];
    }

    for (size_t i = 0; i < tuesdays.size(); ++i) {
        sum_tue = sum_tue + tuesdays[i];
    }

    for (size_t i = 0; i < wednesdays.size(); ++i) {
        sum_wed = sum_wed + wednesdays[i];
    }

    for (size_t i = 0; i < thursdays.size(); ++i) {
        sum_thu = sum_thu + thursdays[i];
    }

    for (size_t i = 0; i < fridays.size(); ++i) {
        sum_fri = sum_fri + fridays[i];
    }

    for (size_t i = 0; i < saturdays.size(); ++i) {
        sum_sat = sum_sat + saturdays[i];
    }                    

    for (size_t i = 0; i < sundays.size(); ++i) {
        sum_sun = sum_sun + sundays[i];
    }


    //cout << 'Results\n';*/
    cout << "Monday Sum: " << sum_mon << '\n';
    cout << "Tuesday Sum: " << sum_tue << '\n';
    cout << "Wednesday Sum: " << sum_wed << '\n';
    cout << "Thursday Sum: " << sum_thu << '\n';
    cout << "Friday Sum:  " << sum_fri << '\n';
    cout << "Saturday Sum: " << sum_sat << '\n';
    cout << "Sunday Sum: " << sum_sun << '\n';

}