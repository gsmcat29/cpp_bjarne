/*
Absolute zero is the lowest temperature that can be reached; it is –273.15°C,
or 0K. The above program, even when corrected, will produce errone-
ous results when given a temperature below this. Place a check in the
main program that will produce an error if a temperature is given below
–273.15°C.
*/

#include <iostream>
using namespace std;

double ctok(double c)   // converts celsis to kelvin
{
    constexpr double KELVIN_VALUE = 273.15;
    double k = c +  KELVIN_VALUE;
    return k;
}

int main()
{
    double c = 0;           // declare input variable
    cin >> c;               // retrieve temperature to input variable
    
    if (c < -273.15) {
        cerr << "ERROR. There is not something lower that absolute zero\n";
        exit(1);
    }

    double k = ctok(c);   // convert temperature
    cout << k << '\n';      // print out temperature
}