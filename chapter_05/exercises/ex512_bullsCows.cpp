/*
Implement a little guessing game called (for some obscure reason) “Bulls
and Cows.” The program has a vector of four different integers in the
range 0 to 9 (e.g., 1234 but not 1122) and it is the user’s task to discover
those numbers by repeated guesses. Say the number to be guessed is 1234
and the user guesses 1359; the response should be “1 bull and 1 cow”
because the user got one digit (1) right and in the right position (a bull)
and one digit (3) right but in the wrong position (a cow). The guessing
continues until the user gets four bulls, that is, has the four digits correct
and in the correct order.
*/
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> number = {1,2,3,4};
    int digit0, digit1, digit2, digit3;
    int bull = 0;
    int cow = 0;
    bool game = true;

    //cout << "Enter 4 digits:\n";

    while(game) {
        cout << "Enter 4 digits:\n";
        cin >> digit0 >> digit1 >> digit2 >> digit3;

        if (digit0 == number[0]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit0)) {
            ++cow;
        }

        if (digit1 == number[1]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit1)) {
            ++cow;
        }

        if (digit2 == number[2]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit2)) {
            ++cow;
        }

        if (digit3 == number[3]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit3)) {
            ++cow;
        }

        // display bulls, cows
        cout << bull << " bull(s) and " << cow << " cow(s)\n";

        if (bull == 4) {
            game = false;
        }
        else if (bull < 4) {
            bull = 0;
            cow = 0;
        }
    }


}