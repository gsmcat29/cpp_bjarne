/*
The program is a bit tedious because the answer is hard-coded into the
program. Make a version where the user can play repeatedly (without
stopping and restarting the program) and each game has a new set of
four digits. You can get four random digits by calling the random number
generator randint(10) from std_lib_facilities.h four times. You will note
that if you run that program repeatedly, it will pick the same sequence
of four digits each time you start the program. To avoid that, ask the
user to enter a number (any number) and call seed_randint(n), also from
std_lib_facilities.h, where n is the number the user entered before calling
randint(10). Such an n is called a seed, and different seeds give different
sequences of random numbers.
*/
#include <algorithm>
#include <iostream>
#include <vector>
#include "std_lib_facilities.h"
using namespace std;

int main()
{
    vector<int> number;
    int digit0, digit1, digit2, digit3;
    int bull = 0;
    int cow = 0;
    bool game = true;
    int seed = 0;
    char answer = ' ';

    while(true) {
        cout << "Do you want to play? Y/N\n";
        cin >> answer;

        if (answer == 'N')
            exit(1);

        cout << "Enter a value for the seed:\n";
        cin >> seed;

        seed_randint(seed);
        cout << '\n';

        // We use this randint(9) to avoid a zero as first value
        randint(9);

        number.push_back(randint(9));
        number.push_back(randint(9));
        number.push_back(randint(9));
        number.push_back(randint(9));

        // to check number at testing
        for (size_t i = 0; i < number.size(); ++i) {
            cout << number[i] << '\t';
        }

        cout << '\n';

    //while(game) {
        cout << "Enter 4 digits:\n";
        cin >> digit0 >> digit1 >> digit2 >> digit3;

        if (digit0 == number[0]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit0)) {
            ++cow;
        }

        if (digit1 == number[1]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit1)) {
            ++cow;
        }

        if (digit2 == number[2]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit2)) {
            ++cow;
        }

        if (digit3 == number[3]) {
            ++bull;
        } else if (count(number.begin(), number.end(), digit3)) {
            ++cow;
        }

        // display bulls, cows
        cout << bull << " bull(s) and " << cow << " cow(s)\n";

        if (bull == 4) {
            //game = false;
            exit(0);
        }
        else if (bull < 4) {
            bull = 0;
            cow = 0;
        }

        number.clear();
    }


}