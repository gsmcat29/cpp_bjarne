/*
Write a program that converts from Celsius to Fahrenheit and from Fahr-
enheit to Celsius (formula in §4.3.3). Use estimation (§5.8) to see if your
results are plausible.
*/

// formula: f = 9/5 * c + 32
// estimation: 

#include <iostream>
using namespace std;

double ctof(double c);
double ftoc(double f);

int main()
{
    double fahr = 0;
    double cels = 0;
    double celsius = 0;
    double fahrenheit = 0;

    cout << "Type temperature in Fahrenheit: ";
    cin >> fahr;

    celsius = ftoc(fahr);
    cout << "That temperature is equal to: " << celsius << '\n';

    cout << "Type temperature in Celsius: ";
    cin >> cels;

    fahrenheit = ctof(cels);
    cout << "That temperature is equal to: " << fahrenheit << '\n';
}


double ftoc(double f)
{
    double c = 0;

    c = ((5/9.0)*(f-32));
    
    return c;
}

double ctof(double c)
{
    double f = 0;

    f = ((9/5.0)*c) + 32;

    return f;
}
