/*
Modify the program from exercise 8 to use double instead of int. Also,
make a vector of doubles containing the N–1 differences between adja-
cent values and write out that vector of differences
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    double sum = 0;
    double values = 0;
    double n = 0;
    double diff = 0;
    vector<double> holder;
    vector<double> adjacent;

    cout << "Please enter the number of values you want to sum:\n";
    cin >> n;

    cout << "Plase enter some integers (press '|' to stop):\n";

    while (cin >> values) {
        holder.push_back(values);
    }

    if (n > holder.size()) {
        cerr << "ERROR: N is greater than vector of numbers\n";
        exit(1);
    }

    cout << "The sum of the first " << n << " numbers ( ";

    for (int i = 0; i < n; ++i) {
        cout << i+1 << " ";
        sum = sum + holder[i];
    }

    cout << ") is " << sum << '\n';

    cout << "Display adjacent values difference:\n";
    for (int j = 0; j < n - 1; ++j) {
        diff = holder[j] - holder[j+1];

        adjacent.push_back(diff);
        cout << adjacent[j] << '\n';

        diff = 0;
    }    
}