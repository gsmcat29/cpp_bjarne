#include <iostream>
using namespace std;

double ctok(double c)   // converts celsis to kelvin
{
    constexpr double KELVIN_VALUE = 273.15;
    double k = c +  KELVIN_VALUE;

    if (c < -273.15) {
        cerr << "ERROR. There is not something lower that absolute zero\n";
        exit(1);
    }

    return k;
}

double ktoc(double k) // convertes kelvin to celsius
{
    constexpr double CELSIUS_VALUE = -273.15;
    double celsius = k + CELSIUS_VALUE;

    return celsius;
}

int main()
{
    double c = 0;           // declare input variable
    double kelvin = 0;
    cout << "Enter value in Celsius: ";
    cin >> c;               // retrieve temperature to input variable
    
    double k = ctok(c);   // convert temperature
    cout << k << '\n';      // print out temperature

    cout << "Enter value in Kelvin: ";
    cin >> kelvin;

    double cels = ktoc(kelvin);
    cout << cels << '\n';
}