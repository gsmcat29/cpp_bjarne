/*
Write a program that writes out the first so many values of the Fibonacci
series, that is, the series that starts with 1 1 2 3 5 8 13 21 34. The next
number of the series is the sum of the two previous ones. Find the largest
Fibonacci number that fits in an int.
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    int f0 = 0;
    int f1 = 1;
    int fn =  0;
    int i = 0;

    int test_fib = 0;

    cout << "Number of Fibonnaci numbers to display: ";
    cin >> test_fib;

    cout << f0 << '\t' << f1 << '\t';

    while( i < test_fib) {
        fn = f0 + f1;
        cout << fn << "\t";

        f0 = f1;
        f1 = fn;

        ++i;
    }

    cout << '\n';

}