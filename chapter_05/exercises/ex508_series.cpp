/*
Write a program that reads and stores a series of integers and then com-
putes the sum of the first N integers. First ask for N, then read the values
into a vector, then calculate the sum of the first N values. For example:

“Please enter the number of values you want to sum:”
3
“Please enter some integers (press '|' to stop):”
12 23 13 24 15 |
“The sum of the first 3 numbers ( 12 23 13 ) is 48.”

Handle all inputs. For example, make sure to give an error message if the
user asks for a sum of more numbers than there are in the vector.
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    int sum = 0;
    int values = 0;
    int n = 0;
    vector<int> holder;

    cout << "Please enter the number of values you want to sum:\n";
    cin >> n;

    cout << "Plase enter some integers (press '|' to stop):\n";

    while (cin >> values) {
        holder.push_back(values);
    }

    if (n > holder.size()) {
        cerr << "ERROR: N is greater than vector of numbers\n";
        exit(1);
    }

    cout << "The sum of the first " << n << " numbers ( ";

    for (int i = 0; i < n; ++i) {
        cout << i+1 << " ";
        sum = sum + holder[i];
    }

    cout << ") is " << sum << '\n';
}