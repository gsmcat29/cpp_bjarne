/*
Modify the program from exercise 8 to write out an error if the result
cannot be represented as an int.
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    double sum = 0;
    double values = 0;
    double n = 0;
    vector<double> holder;

    cout << "Please enter the number of values you want to sum:\n";
    cin >> n;

    cout << "Plase enter some integers (press '|' to stop):\n";

    while (cin >> values) {
        holder.push_back(values);
    }

    if (n > holder.size()) {
        cerr << "ERROR: N is greater than vector of numbers\n";
        exit(1);
    }

    cout << "The sum of the first " << n << " numbers ( ";

    for (int i = 0; i < n; ++i) {
        cout << i+1 << " ";
        sum = sum + holder[i];
    }

    cout << ") is " << sum << '\n';
}