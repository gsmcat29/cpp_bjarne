#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<double> temps;               // temperatures

    for (double temp; cin >> temp;)     // read and put into temps
        temps.push_back(temp);
    
    double sum = 0;
    double high_temp = 0;
    double low_temp = 0;

    for (int x : temps) {
        if (x > high_temp) high_temp = x;   // find high
        if (x < low_temp) low_temp = x;     // find low
        sum += x;
    }

    cout << "High temperature: " << high_temp << '\n';
    cout << "Low temperture: " << low_temp << '\n';
    cout << "Average temperature: " << sum / temps.size() <<  '\n';
}