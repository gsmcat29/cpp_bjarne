/*
Use the example above as a model for a program that converts yen ('y'),
kroner ('k'), and pounds ('p') into dollars. If you like realism, you can find
conversion rates on the web.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double dollar_per_yen = 0.0074;   
    constexpr double dollar_per_kroner = 0.10;
    constexpr double dollar_per_puonds = 1.23;

    double value = 1;                      // length in inches or centimeters
    char unit = ' ';                         // a space is not a unit

    cout << "Please enter a length followed by a unit (y, k, p):\n";
    cin >> value >> unit;

    if (unit == 'y') {
        cout << value << " yen = $" << dollar_per_yen * value << '\n';
    }
    else if (unit == 'k') {
        cout << value << " kroner = $" << dollar_per_kroner * value << '\n';
    }
    else if (unit == 'p') {
        cout << value << " pounds = $" << dollar_per_puonds * value << '\n';
    }
    else {
        cout << "Sorry undefined unit\n";
    }

    return 0;
}