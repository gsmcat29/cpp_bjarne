/*
Implement square() without using the multiplication operator; that is, do the
x*x by repeated addition (start a variable result at 0 and add x to it x times).
Then run some version of “the first program” using that square().
*/

#include <iostream>
using namespace std;

//*********************************************************************
int square(int i)
{
    return i * i;
}

int square2(int i)
{
    int result = 0;

    for(int j = 0; j < i; ++j) {
        result = result + i;
    }

    return result;
}
//*********************************************************************
int main()
{
    int number = 0;

    cout << "Please type a number to square: ";
    cin >> number;

    int ans = square(number);
    int ans2 = square2(number);

    cout << "Square of " << number << " is " << ans << "\n";
    cout << "Square of " << number << " is " << ans2 << "\n";

    return 0;
}