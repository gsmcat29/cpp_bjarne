// for-each vector sample
#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> v = {5,7,9,4,6,8};

    for (int x : v) // for each x in v
        cout << x << '\n';
}