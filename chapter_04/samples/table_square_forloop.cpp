// calculate and print a table of squaeres 0-99
#include <iostream>
using namespace std;

int main()
{
    for (int i = 0; i < 100; ++i)
        cout << i << '\t' << square(i) << '\n';
}