// calculate and print a table of squaeres 0-99
#include <iostream>
using namespace std;

int main()
{
    int i = 0;      // start from 0
    while (i < 100) {
        cout << i << '\t' << square(i) << '\n';
        ++i;
    }

}