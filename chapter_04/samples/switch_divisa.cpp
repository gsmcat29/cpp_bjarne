/*
Rewrite your currency converter program from the previous Try this to use a
switch-statement. Add conversions from yuan and kroner. Which version of
the program is easier to write, understand, and modify? Why?

Use the example above as a model for a program that converts yen ('y'),
kroner ('k'), and pounds ('p') into dollars. If you like realism, you can find
conversion rates on the web.
*/

#include <iostream>
using namespace std;

int main()
{
    constexpr double dollar_per_yen = 0.0074;   
    constexpr double dollar_per_kroner = 0.10;
    constexpr double dollar_per_puonds = 1.23;

    double value = 1;                      // length in inches or centimeters
    char unit = ' ';                         // a space is not a unit

    cout << "Please enter a length followed by a unit (y, k, p):\n";
    cin >> value >> unit;

    switch (unit)
    {
    case 'y':
        cout << value << " yen = $" << dollar_per_yen * value << '\n';
        break;
    case 'k':
        cout << value << " kroner = $" << dollar_per_kroner * value << '\n';
        break;
    case 'p':
        cout << value << " pounds = $" << dollar_per_puonds * value << '\n';
        break;
    default:
        cout << "Sorry undefined unit\n";
        break;
    }

    return 0;
}