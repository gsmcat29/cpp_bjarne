/*
The character 'b' is char('a'+1), 'c' is char('a'+2), etc. Use a loop to write out
a table of characters with their corresponding integer values:
    
    a 97
    b 98
    ...
    z 122

*/

#include <iostream>
using namespace std;

int main()
{
    int numeric = 97;
    char ch = 'a';

    while (numeric < 123) {
        cout << ch << '\t' << numeric << endl;
        ++numeric;
        ch = char(numeric);
    }

    cout << '\n';

    return 0;
}