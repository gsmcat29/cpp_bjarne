// read some temperatures into a vector
#include <algorithm>
#include <iostream>
#include <vector>
#include "std_lib_facilities.h"
using namespace std;

// values to type: 1.2 3.4 5.6 7.8 9.0 |

int main()
{
    vector<double> temps;                 // temperatures
    for (double temp; cin >> temp; )     // read into temp
        temps.push_back(temp);          // put temp into a vector

    // compute mean temperature
    double sum = 0;
    for (double x : temps) sum += x;

    cout << "Average temperature: " << sum / temps.size() << '\n';

    // compute median temperature:
    sort(temps);
    cout << "Median temperature: " << temps[temps.size()/2] << '\n';

    return 0;
}
