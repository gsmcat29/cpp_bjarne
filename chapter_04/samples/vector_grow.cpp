// growing a vector
#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<double> v;       // start off empty, v has no elements

    v.push_back(2.7);       // add an element with value 2.7 at end of v
                            // v nos has one element, v[0] == 2.7

    v.push_back(5.6);       // add an element with value 5.6 at end of v
                            // v = |2.7|5.6|

    v.push_back(7.9);       // add an element with value 7.9 at end of v
                            // v = |2.7|5.6|7.9|

    for (int i = 0; i < v.size(); ++i)
        cout << v[i] << '\t';
    
    cout << '\n';

    return 0;
}