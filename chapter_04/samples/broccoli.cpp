/*
Write a program that “bleeps” out words that you don’t like; that is, you
read in words using cin and print them again on cout. If a word is among a
few you have defined, you write out BLEEP instead of that word. Start with
one “disliked word” such as

    string disliked = “Broccoli”;
*/

#include <iostream>
#include <string>
#include <vector>
#include "std_lib_facilities.h"
using namespace std;

int main()
{
    string disliked = "Broccoli";
    vector<string> words;

    cout << "Please type some words: ";

    for(string temporal; cin >> temporal;) {
        words.push_back(temporal);
    }

    // we sort the words to find brocolli
    for (int i = 0; i < words.size(); ++i) {
        if(words[i] == disliked)
            words[i] = "BLEEP";
        
        cout << words[i] << '\t';
    }

    cout << endl;
}