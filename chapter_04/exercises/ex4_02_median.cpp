/*
If we define the median of a sequence as “a number so that exactly as
many elements come before it in the sequence as come after it,” fix the
program in §4.6.3 so that it always prints out a median. Hint: A median
need not be an element of the sequence.
*/

/* test values:

1, 3, 3, 6, 7, 8, 9 -> median = 6
1, 2, 3, 4, 5, 6, 8, 9 -> median = (4+5)/2 = 4.5
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<double> values;
    double number = 0;
    double median = 0;
    int mid_value = 0;
    double low = 0;
    double high = 0;

    // ask user to enter values
    cout << "Enter different values, type | to end: ";

    while (cin >> number) {
        values.push_back(number);
    }

    mid_value = values.size() / 2;
 
    // when odd median is middle value
    // when even is sum of middle values  / 2
    if(values.size() % 2 == 0) {
        //
        low = mid_value;
        high = values[mid_value];
        median = (low+high) / 2.0;
    }
    else {
        median = values[mid_value];
    }

    cout << "The median is: " << median << "\n";

    return 0;
}