/*
Modify the “mini calculator” from exercise 5 to accept (just) single-digit
numbers written as either digits or spelled out.
*/

#include <iostream>
#include <vector>
#include <string>
using namespace std;

// Function Prototype =======================================================

int suma(string number1, string number2);
int resta(string number1, string number2);
int divd(string number1, string number2);
int mult(string number1, string number2);
int converter(int tester, string index);

// Main Function ============================================================

int main()
{
    string value1, value2;
    int ans = 0;
    char op = ' ';

    cout << "Please enter two numbers and a operation: ";
    cin >> value1 >> value2 >> op;

    switch (op)
    {
    case '+':
        ans = suma(value1, value2);
        cout << "The sum of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    case '-':
        ans = resta(value1, value2);
        cout << "The substration of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    case '*':
        ans = mult(value1, value2);
        cout << "The multiplication of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    case '/':
        ans = divd(value1, value2);
        cout << "The division of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    default:
        cout << "Unknow operation. Please try again\n";
        break;
    }

}


// Function Definition ======================================================
int converter(int tester, string index) 
{
    vector<string> values = {"zero","one","two","three","four","five","six",
                             "seven","eight","nine"};

    vector<string> numeric = {"0","1","2","3","4","5","6","7","8","9"};  

    for (size_t i = 0; i < values.size(); ++i) {
        if (index == values[i])
            tester = i;
        else if (index == numeric[i])
            tester = i;
    }

    return tester;
}


int suma(string number1, string number2)
{
    int value1 = 0, value2 = 0;

    value1 = converter(value1, number1);
    value2 = converter(value2, number2);

    return value1 + value2;
}

int resta(string number1, string number2)
{
    int value1 = 0, value2 = 0;

    value1 = converter(value1, number1);
    value2 = converter(value2, number2);

    return value1 - value2;
}

int divd(string number1, string number2)
{
    int value1 = 0, value2 = 0;

    value1 = converter(value1, number1);
    value2 = converter(value2, number2);

    return value1 / value2;    
}

int mult(string number1, string number2)
{
    int value1 = 0, value2 = 0;

    value1 = converter(value1, number1);
    value2 = converter(value2, number2);

    return value1 * value2;
}