/*
Modify the program from exercise 19 so that when you enter a name, the
program will output the corresponding score or name not found.
*/
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    vector<string> names;
    vector<int> scores;

    string name = " ";
    string lookup = " ";
    int value = 0;
    int flag =  1;

    cout << "Please enter a name, value pair\n";
    cout << "Stop program typing: NoName 0\n";

    while (cin >> name >> value) {
        flag = 1;
        if (name == "NoName" && value == 0)
            break;

        for (string repeat : names) {
            if (repeat == name) {
                cout << "Repeated name!!\n";
                flag = 0;
            }

        }

        if (flag == 1) {
            names.push_back(name);
            scores.push_back(value);
        }
    }

    cout << "Type name to display corresponding score: ";
    cin >> lookup;
    bool not_found = true;
    
    for (size_t i = 0; i < names.size(); ++i) {
        
        if (lookup == names[i]) {
            cout << scores[i] << '\n';
            not_found = false;
        }

    }
    if (not_found == true) 
        cout << "Name not found\n";

    return 0;
}
