/*
Modify the program from exercise 19 so that when you enter an integer,
the program will output all the names with that score or score not found.
*/
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    vector<string> names;
    vector<int> scores;

    string name = " ";
    int value = 0;
    int flag =  1;

    int number = 0;

    cout << "Please enter a name, value pair\n";
    cout << "Stop program typing: NoName 0\n";

    while (cin >> name >> value) {
        flag = 1;
        if (name == "NoName" && value == 0)
            break;

        for (string repeat : names) {
            if (repeat == name) {
                cout << "Repeated name!!\n";
                flag = 0;
            }

        }

        if (flag == 1) {
            names.push_back(name);
            scores.push_back(value);
        }
    }

    cout << "Type number to display corresponding name(s): ";
    cin >> number;
    bool not_found = true;
    
    for (size_t i = 0; i < scores.size(); ++i) {
        
        if (number == scores[i]) {
            cout << names[i] << '\n';
            not_found = false;
        }

    }
    if (not_found == true) 
        cout << "Score not found\n";

    return 0;
}
