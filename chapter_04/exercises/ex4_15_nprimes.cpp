/*
Write a program that takes an input value n and then finds the first n
primes.
*/

#include <iostream>
#include <vector>
using namespace std;


int main()
{
    constexpr int max_value = 100; 
    int n = 0, counter = 0, display = 0;
    cout << "Type n to find the first n primes ";
    cin >> n;

    vector<bool> prime_holder(max_value+1, true);

    // 0 and 1 are not prime numbers
    prime_holder[0] = false;
    prime_holder[1] = false;
    
 
    for (int i = 2; i < max_value; ++i) {
        if (prime_holder[i] && i * i <= max_value) {
            for (int j = i * i; j <= max_value; j += i)
                prime_holder[j] = false;
        }

        ++counter;

        if (counter >= n)
            break;
    }

    // Display result
    cout << "Final vector:" << '\n';
    for (size_t k = 0; k < max_value; ++k) {
        if (prime_holder[k]) {
            cout << k << " ";
            ++display;

            if (display >= n)
                break;
        }
        
    }
        
    cout << '\n';
}
