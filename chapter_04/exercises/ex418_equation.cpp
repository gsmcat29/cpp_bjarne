/*
Write a program to solve quadratic equations. A quadratic equation is of
the form:


    ax 2 + bx + c = 0
If you don’t know the quadratic formula for solving such an expression,
do some research. Remember, researching how to solve a problem is of-
ten necessary before a programmer can teach the computer how to solve
it. Use doubles for the user inputs for a, b, and c. Since there are two
solutions to a quadratic equation, output both x1 and x2.
*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double a = 0;
    double b = 0;
    double c = 0;
    // quadratic formula
    double x0 = 0;
    double x1 = 0;

    cout << "Please enter the arguments for a, b, and c: ";
    cin >> a >> b >> c;

    double discriminant = (b * b) - 4*(a * c);

    if (discriminant > 0) {
        cout << "We are going to obtain two real solutions:\n";
        x0 = (-b + sqrt(discriminant)) / (2 * a);
        x1 = (-b - sqrt(discriminant)) / (2 * a);

        cout << "Root0 = " << x0 << '\n';
        cout << "Root1 = " << x1 << '\n';
    }
    else if (discriminant == 0) {
        cout << "We are going to obtain one solution:\n";
        x0 = (-b + sqrt(discriminant)) / (2 * a);
        cout << "Root: " << x0 << '\n';
    }
    else {
        cout << "We are going to obtain complex solution:\n";
        cout << "This program does not support complex numbers!!\n";
        exit(1);
    }


}