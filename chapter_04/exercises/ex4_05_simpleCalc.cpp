/*
Write a program that performs as a very simple calculator. Your calculator
should be able to handle the four basic math operations — add, subtract,
multiply, and divide — on two input values. Your program should prompt
the user to enter three arguments: two double values and a character to
represent an operation. If the entry arguments are 35.6, 24.1, and '+', the
program output should be The sum of 35.6 and 24.1 is 59.7. In Chapter 6
we look at a much more sophisticated simple calculator.
*/

#include <iostream>
using namespace std;

double suma(double number1, double number2);
double resta(double number1, double number2);
double div(double number1, double number2);
double mult(double number1, double number2);

// =============================================================

int main()
{
    double value1 = 0, value2 = 0;
    double ans = 0;
    char op = ' ';

    cout << "Please enter two numbers and a operation: ";
    cin >> value1 >> value2 >> op;

    switch (op)
    {
    case '+':
        ans = suma(value1, value2);
        cout << "The sum of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    case '-':
        ans = resta(value1, value2);
        cout << "The substration of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    case '*':
        ans = mult(value1, value2);
        cout << "The multiplication of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    case '/':
        ans = div(value1, value2);
        cout << "The division of " << value1 << " and " << value2 << " is "  << ans;
        cout << '\n';
        break;
    default:
        cout << "Unknow operation. Please try again\n";
        break;
    }

}


// ==============================================================
double suma(double number1, double number2)
{
    return number1 + number2;
}

double resta(double number1, double number2)
{
    return number1 - number2;
}

double div(double number1, double number2)
{
    return number1 / number2;
}

double mult(double number1, double number2)
{
    return number1 * number2;
}