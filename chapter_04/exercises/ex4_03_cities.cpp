/*
Read a sequence of double values into a vector. Think of each value as
the distance between two cities along a given route. Compute and print
the total distance (the sum of all distances). Find and print the smallest
and greatest distance between two neighboring cities. Find and print the
mean distance between two neighboring cities.
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<double> distances;
    double largest_distance = 0;
    double smallest_distance = 0;
    double sum_distances = 0;
    double mean = 0;
    double values = 0;

    cout << "Enter a set of distances to compute (end with pipe): ";
    while (cin >> values) {
        distances.push_back(values);
    }

    smallest_distance = distances[0];

    for (size_t i = 0; i < distances.size(); ++i) {
        // sum of distances
        sum_distances = sum_distances + distances[i];

        if (distances[i] < smallest_distance) {
            smallest_distance = distances[i];
        }
        else if (distances[i] > largest_distance) {
            largest_distance = distances[i];
        }
    }

    // mean = average
    mean = sum_distances / distances.size();

    // display results
    cout << "Sum of distances: " << sum_distances << '\n';
    cout << "Mean of distances: " << mean << '\n';
    cout << "Smallest distance: " << smallest_distance << '\n';
    cout << "Largest distance: " << largest_distance << '\n';

}