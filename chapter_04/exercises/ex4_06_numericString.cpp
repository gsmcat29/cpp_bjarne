/*
Make a vector holding the ten string values "zero", "one", . . . "nine".
Use that in a program that converts a digit to its corresponding
spelled-out value; e.g., the input 7 gives the output seven. Have the same
program, using the same input loop, convert spelled-out numbers into
their digit form; e.g., the input seven gives the output 7.
*/

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<string> values = {"zero","one","two","three","four","five","six",
                             "seven","eight","nine"};

    vector<string> numeric = {"0","1","2","3","4","5","6","7","8","9"};
    
    string number = " ";
    string number_string = " ";

    cout << "Please type a number from 0 - 9: ";
    
    while(cin >> number) {
        
        for (size_t i = 0; i < values.size(); ++i) {
            if (number == values[i])
                cout << "Ans: " << i << '\n';
            else if (number == numeric[i])
                cout << "Ans: " << values[i] << '\n';
        }
    }

 
    cout << "\n";
}