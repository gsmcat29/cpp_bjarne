/*
In the drill, you wrote a program that, given a series of numbers, found
the max and min of that series. The number that appears the most times
in a sequence is called the mode. Create a program that finds the mode of
a set of positive integers.
*/

#include <iostream>
#include <vector>
#include "std_lib_facilities.h"
using namespace std;

int main()
{
    vector<int> sequence;
    vector<int> counter;
    int value = 0;  

    cout << "Enter the values for the sequence: ";

    while (cin >> value) {
        sequence.push_back(value);
    }

    sort(sequence);

    // check sequence
    for (size_t i = 0; i < sequence.size(); ++i)
        cout << sequence[i] << " ";

    // count values
    int current_value = 0;
    int mode = 0;
    int temp = 0;

    for (size_t j = 0; j < sequence.size(); ++j) {
        current_value = count(sequence.begin(), sequence.end(), sequence[j]);

        if (current_value > temp)
            mode = sequence[j];
    }

    cout << "\nThe mode values is: " << mode << '\n';
    cout << "Min value: " << sequence[0] << '\n';
    cout << "Max value: " << sequence[sequence.size()-1] << "\n";

    return 0;
}