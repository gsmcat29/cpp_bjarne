/*
Create a program to find all the prime numbers between 1 and 100. One
way to do this is to write a function that will check if a number is prime
(i.e., see if the number can be divided by a prime number smaller than
itself) using a vector of primes in order (so that if the vector is called
primes, primes[0]==2, primes[1]==3, primes[2]==5, etc.). Then write a
loop that goes from 1 to 100, checks each number to see if it is a prime,
and stores each prime found in a vector. Write another loop that lists the
primes you found. You might check your result by comparing your vector
of prime numbers with primes. Consider 2 the first prime.
*/

#include <iostream>
#include <vector>
using namespace std;

int is_prime(int n);

int main()
{
    vector<int> prime_holder;
    
    for (int i = 2; i < 100; ++i) {
        if (is_prime(i))
            prime_holder.push_back(i);
    }

    cout << "Final vector:" << '\n';
    for (size_t j = 0; j < prime_holder.size(); ++j)
        cout << prime_holder[j] << " ";

    cout << '\n';
}


int is_prime(int n) 
{
    int value = 0;
    bool primo = true;

    for (int i = 2; i <= n / 2; ++i) {

        if (n % i == 0) {
            primo = false;
            break;
        }

    }
    
    if(primo)
        value = n;

    return value;
}