/*
Write a program to play a numbers guessing game. The user thinks of a
number between 1 and 100 and your program asks questions to figure
out what the number is (e.g., “Is the number you are thinking of less than
50?”). Your program should be able to identify the number after asking
no more than seven questions. Hint: Use the < and <= operators and the
if-else construct.
*/

#include <iostream>
using namespace std;

int main()
{
    int number_to_guess = 0;
    int higher_bound = 100;
    int lower_bound = 1;
    int counter = 0;
    int temp = 0;
    bool guess_state = false;
    char answer = ' ';
    char guess = ' ';

    cout << "Please think of a number: ";

    int current = higher_bound / 2;

    while(counter < 7 && guess_state == false) {
        cout << "Is your number  less than " << current << "?\n";

        cin >> answer;

        temp = current;

        if (answer == 'y') {
            higher_bound = current;
            current = (current / 2) - 1;

        }
        else if (answer == 'n') {
            lower_bound = current;
            current = current + (higher_bound - lower_bound) + 1;
        }

        ++counter;
    }
}