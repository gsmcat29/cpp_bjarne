/*
Modify the program described in the previous exercise to take an input
value max and then find all prime numbers from 1 to max
*/

#include <iostream>
#include <vector>
using namespace std;

int is_prime(int n);
//***************************************************************************
int main()
{
    int max_prime = 0;
    vector<int> prime_holder;

    cout << "Type max number of primes to calculate: ";
    cin >> max_prime;
    
    for (int i = 2; i < max_prime; ++i) {
        if (is_prime(i))
            prime_holder.push_back(i);
    }

    cout << "Final vector:" << '\n';
    for (size_t j = 0; j < prime_holder.size(); ++j)
        cout << prime_holder[j] << " ";

    cout << '\n';
}

//***************************************************************************
int is_prime(int n) 
{
    int value = 0;
    bool primo = true;

    for (int i = 2; i <= n / 2; ++i) {

        if (n % i == 0) {
            primo = false;
            break;
        }

    }
    
    if(primo)
        value = n;

    return value;
}