/*
Write a program that plays the game “Rock, Paper, Scissors.” If you are not
familiar with the game do some research (e.g., on the web using Google).
Research is a common task for programmers. Use a switch-statement to
solve this exercise. Also, the machine should give random answers (i.e.,
select the next rock, paper, or scissors randomly). Real randomness is too
hard to provide just now, so just build a vector with a sequence of values
to be used as “the next value.” If you build the vector into the program,
it will always play the same game, so maybe you should let the user en-
ter some values. Try variations to make it less easy for the user to guess
which move the machine will make next.
*/

#include <iostream>
#include <vector>
using namespace std;


int main()
{
    vector<char> opponent = {'r','p','s'};
    int counter = 0;
    char my_try = ' ';

    int my_wins = 0;
    int opponent_wins = 0;
    int ties = 0;

    cout << "Enter " << opponent.size() << " inputs. End with pipe |: ";

    while(cin >> my_try) {
        
        switch (my_try)
        {
        case 'r':
            if(opponent[counter] == 'r')
                ++ties;
            else if (opponent[counter] == 'p')
                ++opponent_wins;
            else if (opponent[counter] == 's')
                ++my_wins;
            break;
        case 'p':
            if(opponent[counter] == 'p')
                ++ties;
            else if (opponent[counter] == 'r')
                ++my_wins;
            else if (opponent[counter] == 's')
                ++opponent_wins;
            break;
        case 's':
            if(opponent[counter] == 's')
                ++ties;
            else if (opponent[counter] == 'p')
                ++my_wins;
            else if (opponent[counter] == 'r')
                ++opponent_wins;
            break;            
        default:
            break;
        }

        ++counter;
    }

    cout << "Results:\n";
    cout << "My Wins: " << my_wins << '\n';
    cout << "Opponent Wins: " << opponent_wins << '\n';
    cout << "Ties: " << ties << '\n';

    return 0;
}