/*
Create a program to find all the prime numbers between 1 and 100. There
is a classic method for doing this, called the “Sieve of Eratosthenes.” If
you don’t know that method, get on the web and look it up. Write your
program using this method.
*/

#include <iostream>
#include <vector>
using namespace std;

//int is_prime(int n);

int main()
{
    constexpr int LIMIT = 100;
    vector<bool> prime_holder(LIMIT+1, true);

    // 0 and 1 are not prime numbers
    prime_holder[0] = false;
    prime_holder[1] = false;
    
 
    for (int i = 2; i < LIMIT; ++i) {
        if (prime_holder[i] && i * i <= LIMIT) {
            for (int j = i * i; j <= LIMIT; j += i)
                prime_holder[j] = false;
        }
    }



    // Display result
    cout << "Final vector:" << '\n';
    for (size_t k = 0; k < prime_holder.size(); ++k) {
        if (prime_holder[k])
            cout << k << " ";
    }
        

    cout << '\n';
}


