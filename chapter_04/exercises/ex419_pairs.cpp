/*
Write a program where you first enter a set of name-and-value pairs, such
as Joe 17 and Barbara 22. For each pair, add the name to a vector called
names and the number to a vector called scores (in corresponding po-
sitions, so that if names[7]=="Joe" then scores[7]==17). Terminate input
with NoName 0. Check that each name is unique and terminate with an
error message if a name is entered twice. Write out all the (name,score)
pairs, one per line
*/
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    vector<string> names;
    vector<int> scores;

    string name = " ";
    int value = 0;
    int flag =  1;

    cout << "Please enter a name, value pair\n";
    cout << "Stop program typing: NoName 0\n";

    while (cin >> name >> value) {
        flag = 1;
        if (name == "NoName" && value == 0)
            break;

        for (string repeat : names) {
            if (repeat == name) {
                cout << "Repeated name!!\n";
                flag = 0;
            }

        }

        if (flag == 1) {
            names.push_back(name);
            scores.push_back(value);
        }
    }

    cout << "Final pair (name, value):\n";
    for (size_t i = 0; i < names.size(); ++i) {
        // code in here
        cout << "(" << names[i] << " , " << scores[i] << ")\n";

    }

    return 0;
}
