/*
Try to calculate the number of rice grains that the inventor asked for in
exercise 8 above. You’ll find that the number is so large that it won’t fit
in an int or a double. Observe what happens when the number gets too
large to represent exactly as an int and as a double. What is the larg-
est number of squares for which you can calculate the exact number of
grains (using an int)? What is the largest number of squares for which
you can calculate the approximate number of grains (using a double)?
*/


#include <iostream>
using namespace std;

int main()
{
    constexpr double SQUARES = 64;
    double grains = 1;
    double init_squares = 1;

    while (init_squares < SQUARES) {
        grains = grains * 2;
        ++init_squares;
    }

    cout << "Number of grains : " << grains << '\n';

    return 0;
}