/*
Write a program that finds the min, max, and mode of a sequence of
strings.
*/
#include <iostream>
#include <vector>
#include <string>
#include "std_lib_facilities.h"
using namespace std;

int main()
{
    vector<string> sequence;
    string word;  

    cout << "Enter the values for the sequence: ";

    while (cin >> word) {

        if (word == "|")
            break;

        sequence.push_back(word);
    }

    sort(sequence);
    cout << "--" << sequence[0] << "\n";

    // check sequence
    for (size_t i = 0; i < sequence.size(); ++i)
        cout << sequence[i] << " ";

    // count values
    cout << '\n';
    
    int current_value = 0;
    string mode;
    int temp = 1;

    for (size_t j = 0; j < sequence.size(); ++j) {
        current_value = count(sequence.begin(), sequence.end(), sequence[j]);

        if (current_value > temp) {
            mode = sequence[j];
        }
    }

    cout << "\nThe mode values is: " << mode << '\n';
    
    cout << "Min value: " << sequence[0] << '\n';
    cout << "Max value: " << sequence[sequence.size()-1] << "\n";

    return 0;
}