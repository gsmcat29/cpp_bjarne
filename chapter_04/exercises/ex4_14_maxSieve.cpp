/*
Modify the program described in the previous exercise to take an input
value max and then find all prime numbers from 1 to max.
*/

#include <iostream>
#include <vector>
using namespace std;


int main()
{
    int max_value = 0; 

    cout << "Type max number for prime number sequence: ";
    cin >> max_value;

    vector<bool> prime_holder(max_value+1, true);

    // 0 and 1 are not prime numbers
    prime_holder[0] = false;
    prime_holder[1] = false;
    
 
    for (int i = 2; i < max_value; ++i) {
        if (prime_holder[i] && i * i <= max_value) {
            for (int j = i * i; j <= max_value; j += i)
                prime_holder[j] = false;
        }
    }

    // Display result
    cout << "Final vector:" << '\n';
    for (size_t k = 0; k < prime_holder.size(); ++k) {
        if (prime_holder[k])
            cout << k << " ";
    }
        
    cout << '\n';
}
