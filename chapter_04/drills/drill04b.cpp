#include <iostream>
#include <string>
#include <vector>
#include "std_lib_facilities.h"
using namespace std;


// from task 6 and so on 
bool units(string unit) 
{
    // function to accept certain units to process
    bool approved = false;
    if (unit == "cm" || unit == "m" || unit == "in" || unit == "ft")
        approved = true;
    else {
        cout << "Illegal unit\n";
        cout << "This program just accepts cm, m, in, ft\n";
        exit(1);
    }
    return approved;
}

// function to convert everything to meters to later compare smalles vs largest
double convert_meters(double amount, string unit)
{
    constexpr double CM_TO_M = 0.01;
    constexpr double IN_TO_M = 0.0254;
    constexpr double FT_TO_M = 0.3048;

    double meters = 0;

    if (unit == "cm") {
        meters = amount * CM_TO_M;
    }
    else if (unit == "in") {
        meters = amount * IN_TO_M;
    }
    else if (unit == "ft") {
        meters = amount * FT_TO_M;
    }
    else if (unit == "m") {
        meters = amount;
    }

    return meters;
}

int main()
{
    double number1 = 0;
    double smallest = 0;
    double largest  = 0;
    double value = 0;
    double sum = 0;
    
    int number_of_values = 0;
    
    string unit = "";

    vector<double> entered_meters;


    cout << "Type a number and a unit, type ctrl+d to finish:\n";


    while (cin >> number1 >> unit) {

        units(unit);

        value = convert_meters(number1, unit);

        sum = sum + value;

        smallest = value;

        entered_meters.push_back(value);

        if (number1 > largest) {
            largest = value;
            cout << "Largest so far: " << largest << "\n";
        }
        else if (number1 < largest) {
            smallest = value;
            cout << "Smallest so far : " << smallest << "\n";
        }

        ++number_of_values;
    }

    cout << "Total number of values: " << number_of_values << "\n";
    cout << "Sum of values: " << sum << "\n";

    sort(entered_meters);

    for (size_t i = 0; i < entered_meters.size(); ++i)
        cout << entered_meters[i] << "\t";

    cout << "\n";
}