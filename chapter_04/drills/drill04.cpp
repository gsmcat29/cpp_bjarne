#include <iostream>
#include <vector>
using namespace std;

int main()
{
    double number1 = 0, number2 = 0;

    cout << "Type a pair of values, type | to finish:\n";

    while (cin >> number1 >> number2) {
        //cout << number1 <<", " << number2 << '\n';
        if (number1 >  number2) {
            cout << "The smaller number is: " << number2 << '\n';
            cout << "The larger value is: " << number1 << '\n';
            if (number1 - number2 <= 0.02) {
                cout << "The numbers are almost equal\n";
            }
        }
        else if (number2 >  number1) {
            cout << "The smaller number is: " << number1 << '\n';
            cout << "The larger value is: " << number2 << '\n';
            if (number2 - number1 <= 0.02) {
                cout << "The numbers are almost equal\n";
            }
        }
        else if (number1 == number2) {
            cout << "The numbers are equal\n";
        }

    }

}